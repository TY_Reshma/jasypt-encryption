package com.te.jasyptencryption.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.jasyptencryption.entity.Student;

@Repository

public interface StudentRepository extends JpaRepository<Student, Integer> {

}
