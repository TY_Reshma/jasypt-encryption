package com.te.jasyptencryption.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.jasyptencryption.entity.Student;
import com.te.jasyptencryption.repository.StudentRepository;

@Service
public class StudentService {
	
	@Autowired(required = true)
	private StudentRepository studentRepository;

	public List<Student> getAllStudent() {
		return studentRepository.findAll();
	}
}
